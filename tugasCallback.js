class Table {
    constructor(init) {
      this.init = init;
    }
  
    createHeader(data) {
      let open = "<thead><tr>";
      let close = "</tr></thead>";
      data.forEach((d) => {
        open += `<th>${d}</th>`;
      });
  
      return open + close;
    }
  
    createBody(data) {
      let open = "<tbody>";
      let close = "</tbody>";
  
      data.forEach((d) => {
        open += `
          <tr>
            <td>${d[0]}</td>
            <td>${d[1]}</td>
            <td>${d[2]}</td>
            <td>${d[3]}</td>
            <td>${d[4]}</td>
            <td>${d[5]}</td>
            <td>${d[6]}</td>
            <td>${d[7]}</td>
          </tr>
        `;
      });
  
      return open + close;
    }
  
    render(element) {
      let table =
        "<table class='table table-hover border border-3'>" +
        this.createHeader(this.init.columns) +
        this.createBody(this.init.data) +
        "</table>";
      element.innerHTML = table;
    }
  }

  function getData(url, cb){
    let xhr = new XMLHttpRequest();
    xhr.onload = function(){
        if(xhr.status === 200){
            return cb(JSON.parse(xhr.responseText));
        }
    }; xhr.open("get", url);
    xhr.send();
}
  
  const data = getData("https://jsonplaceholder.typicode.com/users", data => {
    const app = document.getElementById("app");
    const columns = ["id", "name", "username", "email", "address", "phone", "website", "company"]
    let dataTable = []
    data.map(item => {
        let tableData = [item.id, item.name, item.username, item.email, item.address["city"], item.phone, item.website, item.company["name"]]
        dataTable.push(tableData)
    })
    const table = new Table({
        columns: columns,
        data: dataTable
    })
    console.log(dataTable)
    table.render(app)
  })
  
  
  
  